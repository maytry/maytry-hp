var path = require('path');
var webpack = require('gulp-webpack');
var BowerWebpackPlugin = require("bower-webpack-plugin");

module.exports = {
  root: "/",
  // エントリーポイント
  entry: "./dist/app.js",
  dst: "./dist",
  output: {
    filename: "bundle.js"
  },
  
  // 依存関係
  resolve: {
//    root: [path.join(process.cwd(), 'bower_components')],
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.json', '.html', '.ts'],
    alias: {
      bower: "bower_components",
      ngMaterial: __dirname + "/bower_components/angular-material/index.js",
      ngBootstrap: __dirname + "/bower_components/angular-bootstrap/mt-index.js",
      uiRouter: __dirname + "/bower_components/angular-ui-router/release/angular-ui-router.min.js",
      lodash: __dirname + "/bower_components/lodash/lodash.js"
    }
  },   
  // bowerで取得したライブラリの読み込み用プラグイン
  plugins: new BowerWebpackPlugin()
};