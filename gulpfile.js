var gulp = require("gulp");
var rimraf = require("rimraf");
var typescript = require("gulp-typescript");
var sass = require("gulp-sass");
var watch = require("gulp-watch");
var plumber = require("gulp-plumber");
var webpack = require("gulp-webpack");
var tslint = require("gulp-tslint");
var webpackConfig = require("./webpack.config");
var runSequence = require('run-sequence');

var config = {
  view: "dist/views/**",
  rimraf: {
    src: "dist/**/*.*"
  },
  copy: {
    src: "src/**",
    exclude: {
      ts: "!src/**/*.ts",
      sass: "!src/styles/**/*.scss"
    },
    dst: "dist"
  },

  ts: {
    src: "src/**/*.ts",
    exclude: "!src/bower_components/**/*.ts",
    dst: "dist",
    options: {
      target: "ES5",
      module: "commonjs"
    }
  },

  sass: {
    src: "src/styles/**/*.scss",
    dst: "dist/styles"
  }
};

gulp.task("build", function(callback) {
  runSequence("compile", "copy", callback);
});

gulp.task("compile", function(callback) {
  runSequence("tsc", ["webpack", "sass"], callback);
});

gulp.task("webpack", function() {
  gulp.src(webpackConfig.entry)
    .pipe(plumber())
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest(webpackConfig.dst));
});

gulp.task("clean", function(callback) {
  rimraf(config.rimraf.src, callback);
});

gulp.task("copy", function() {
  gulp.src([config.copy.src, config.copy.exclude.ts, config.copy.exclude.sass])
    .pipe(plumber())
    .pipe(gulp.dest(config.copy.dst));
});

gulp.task("tsc", ["tslint"], function() {
  gulp.src([config.ts.src, config.ts.exclude])
    .pipe(typescript(config.ts.options))
    .js
    .pipe(gulp.dest(config.ts.dst));
});

gulp.task('tslint', function(){
  gulp.src(config.ts.src)
    .pipe(plumber())
    .pipe(tslint())
    .pipe(tslint.report('verbose'));
});

gulp.task("sass", function() {
  gulp.src(config.sass.src)
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest(config.sass.dst));
});

gulp.task("watch", function() {
  watch(config.ts.src, function(callback) {
    gulp.start(["tsc"]);
  });

  watch(config.copy.src, function () {
    gulp.start(["copy"]);
  });

  watch(config.sass.src, function () {
    gulp.start(["sass"]);
  });

  // js
  watch(webpackConfig.entry, function () {
    gulp.start(["webpack"]);
  });
});