var app = angular.module("app");
app.config(($stateProvider:any) => {
  $stateProvider
    .state("blog", {
      url: "/blog",
      templateUrl: "views/blog/index.html"
    })
    .state("blog.top", {
      views: {
        header: {
          controller: "blogHeaderController",
          templateUrl: "views/blog/header.html"
        },
        left: {
          controller: "blogLeftColumnController",
          templateUrl: "views/blog/leftColumn.html"
        },
        form: {
          controller: "blogFormController",
          templateUrl: "views/blog/form.html"
        },
        top: {
          controller: "blogTopController",
          templateUrl: "views/blog/top/index.html"
        }
      }
    })
    .state("blog.top.timeline", {
      url: "/timeline",
      controller: "blogTimelineController",
      templateUrl: "views/blog/top/timeline.html"
    })
    .state("blog.top.murmur", {
      url: "/murmur",
      templateUrl: "views/blog/top/murmur.html"
    })
    .state("blog.top.article", {
      url: "/article",
      templateUrl: "views/blog/top/article.html"
    });
});
export = app;
