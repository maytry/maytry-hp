var app = angular.module("app");
app.config(($stateProvider:any, $urlRouterProvider:any) => {
  $urlRouterProvider
    .when("", "/top");

  $stateProvider
    .state("home", {
      url: "",
      controller: "homeController",
      templateUrl: "views/home/index.html"
    })
    .state("home.top", {
      url: "/top",
      templateUrl: "views/home/top/index.html"
    })
    .state("home.second", {
      url: "/second",
      templateUrl: "views/home/second/index.html"
    })
    .state("home.third", {
      url: "/third",
      templateUrl: "views/home/third/index.html"
    });
});

export = app;
