/**
 * Created by renma on 8/28/15.
 */

/// <reference path="../typings/tsd.d.ts" />

import ng = require("angular");
import ngAnimate = require("angular-animate");

var app = ng.module("app",
  [ngAnimate,
    require("ngMaterial"),
    require("ngBootstrap"),
    require("uiRouter")]);

/** Service */
import "./plugins/factories/genreService";

/** Filter */
import "./plugins/filters/joinNames";

/** Controller */
import "./controllers/bundle";

/** Directive */
import "./plugins/directives/bundle";

/** Router */
import "./routers/home/index";
import "./routers/blog/index";

app.config(($stateProvider:any, $urlRouterProvider:any) => {
  $urlRouterProvider
    .otherwise("/top");
});
