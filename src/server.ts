import express = require("express");

var app = express();
var appRoot = __dirname;

app.set("port", process.env.PORT || 9000);
app.use(express.static(appRoot));
app.get("/", (req:any, res:any) => {
  res.sendFile(appRoot + "/index.html");
});

app.listen(app.get("port"), () => {
  console.log("Express server listening on port %d in %s mode", app.get("port"), app.settings.env);
});
