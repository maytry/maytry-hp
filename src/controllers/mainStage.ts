/**
 * Created by renma on 9/5/15.
 */
import angular = require("angular");

interface MainStageScope {
  onClickSlidePrev: Function;
  onClickSlideNext: Function;
  onClickStageNext: Function;
}

class MainStageController {
  constructor(private $scope: MainStageScope) {
    $scope.onClickSlidePrev = angular.bind(this, this.onClickSlidePrev);
    $scope.onClickSlideNext = angular.bind(this, this.onClickSlideNext);
    $scope.onClickStageNext = angular.bind(this, this.onClickStageNext);
  }

  onClickSlidePrev(): void {
    console.log("slidePrev");
  }

  onClickSlideNext(): void {
    console.log("slideNext");
  }

  onClickStageNext(): void {
    console.log("stageNext");
  }
}

var app = angular.module("app");
app.controller("mainStageController", ["$scope", MainStageController]);
export = app;
