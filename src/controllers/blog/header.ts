/**
 * Created by renma on 9/5/15.
 */

interface BlogHeaderScope {
  onClickSlidePrev:Function;
  headerMenus:string[];
}

class BlogHeaderController {

  constructor(private $scope: BlogHeaderScope) {
    $scope.onClickSlidePrev = angular.bind(this, this.onClickSlidePrev);

    $scope.headerMenus = ["Home", "MyPage", "Concept", "Contact"];
  }

  onClickSlidePrev():void {
    console.log("slidePrev");
  }
}

var app = angular.module("app");
app.controller("blogHeaderController", ["$scope", BlogHeaderController]);
export = app;
