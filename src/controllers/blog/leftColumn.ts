/**
 * Created by renma on 9/5/15.
 */

import _ = require("lodash");
import Genre = require("src/plugins/fragments/genre");

interface BlogLeftColumnScope {
  selectedGenreHeader:string;
  selectedGenres:Genre[];
  genreHeader:string;
  allGenre:Genre[];
  isHoverCheckBox:boolean;
  removeSelectedGenre:Function;
  addSelectedGenre:Function;
}

class BlogLeftColumnController {

  constructor(private $scope:BlogLeftColumnScope, private genreService:any) {
    $scope.selectedGenreHeader = "選択カテゴリ";
    $scope.selectedGenres = genreService.selectedGenres;
    $scope.genreHeader = "カテゴリ";
    $scope.allGenre = genreService.allGenre;
    $scope.removeSelectedGenre = angular.bind(this, this.removeSelectedGenre);
    $scope.addSelectedGenre = angular.bind(this, this.addSelectedGenre);
  }

  /** 選択ジャンルを削除 */
  removeSelectedGenre(event:any, index:number):void {
    this.genreService.selectedGenres[index].selected = false;
    this.genreService.selectedGenres.splice(index, 1);
  }

  /** ジャンルを選択ジャンルに追加 */
  addSelectedGenre(event:any, clickedGenre:Genre):void {
    var containsGenre:boolean = false;
    _.each(this.genreService.selectedGenres, (selectedGenre:Genre) => {
      if(selectedGenre.id === clickedGenre.id) {
        containsGenre = true;
        selectedGenre.selected = true;
      }
    });
    if(!containsGenre) {
      clickedGenre.selected = true;
      this.genreService.selectedGenres.push(clickedGenre);
    }
  }
}

var app = angular.module("app");
app.controller("blogLeftColumnController", ["$scope", "genreService", BlogLeftColumnController]);
export = app;
