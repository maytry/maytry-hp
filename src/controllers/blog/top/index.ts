/**
 * Created by renma on 9/5/15.
 */

import Genre = require("src/plugins/fragments/genre");

interface TabMenu {
  link:string;
  title:string;
  disabled:boolean;
}

interface BlogTopScope {
  tabMenus:TabMenu[];
  selectedGenres:Genre[];
}

class BlogTopController {

  constructor(private $scope: BlogTopScope, private genreService:any) {
    $scope.tabMenus = [
      { link: "#/blog/timeline", title: "タイムライン", disabled: false},
      { link: "#/blog/murmur", title: "つぶやき", disabled: false},
      { link: "#/blog/article", title: "記事", disabled: false}
    ];

    $scope.selectedGenres = genreService.selectedGenres;
  }
}

var app = angular.module("app");
app.controller("blogTopController", ["$scope", "genreService", BlogTopController]);
export = app;
