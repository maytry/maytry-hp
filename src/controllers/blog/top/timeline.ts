/**
 * Created by renma on 9/5/15.
 */

import _ = require("lodash");

interface User {
  id:string;
  name:string;
  icon:string;
}

interface BlogStreamItemParam {
  id:string;
  users:User[];
  title:string;
  writeTime:Date;
  diffTime:string;
  content:string;
  commentNum:number;
  favoriteNum:number;
  shareNum:number;
}

interface BlogTimelineScope {
  blogStreamItemParams:BlogStreamItemParam[];
  getBlogStreamItemPath:Function;
  joinUserNames:Function;
  infiniteItems:any;
  loadMore:Function;
  getPath:Function;
}

class BlogTimelineController {
  constructor(private $scope:BlogTimelineScope, $timeout:any) {
    var testData:any = [
      {
        id: "demo01",
        users: [
          {id: "0000", name: "れんれん", icon: "renren"}
        ],
        title: "",
        writeTime: new Date,
        diffTime: "3時間前",
        content: "会社が腐ってて社員が泣きを見るのはなんとも悲しいけども、辞めても食っていけるわって言ってスパッと辞められる（能力のある）人材になりたいね＾～＾",
        commentNum: 3,
        favoriteNum: 3,
        shareNum: 3
      }, {
        id: "demo02",
        users: [
          {id: "0000", name: "れんれん", icon: "renren"},
          {id: "0001", name: "かっぽよ", icon: "kappoyo"}
        ],
        title: "ksnctfをちょこちょこやってみた",
        writeTime: new Date,
        diffTime: "",
        content: "ksnctfをマイペースでちょこちょこやってます。初見で解けたものはあんまりないけど、ぐぐったりして結構わかるもんですね。全部ではないですが、自分なりのwrite upを載せておきます。\n" +
        "Q7. Programming\n" +
        "スペースが何を意味しているのか…トリビアですね。\n" +
        "Q9. Digest is secure!\n" +
        "これはすごい勉強になりました。Digest認証とはなんなのかとにかく調べる。後はどうやってリクをごにょごにょ…\n" +
        "Q12. Hypertext Preprocessor\n" +
        "PHPのCGI\n" +
        "スクリプト実行\n" +
        "ディレクトリ検索\n" +
        "Q13. Proverb\n" +
        "個人的にはハッキング！って感じを得られる楽しい問題でした。ファイルはコピーできないnoni\n" +
        "/tmpを使うってことは、/tmp以下に上手く…\n" +
        "Q16. Math I\n" +
        "RSA暗号。高校数学(中学数学？)で解けます。\n" +
        "Q17. Math II\n" +
        "普通に探索したら時間がかかりすぎます。\n" +
        "Q22. Square Cipher\n" +
        "これは気付けたら嬉しいですね。メモ帳じゃキツいかな。\n" +
        "Q26. Sherlock Holmes\n" +
        "面白い、というか、こんなに簡単に見られると怖いですね。コマンドを知っていればきっと…\n" +
        "Q32. Simple Auth\n" +
        "こんな脆弱性のある関数を認証に使ってはいけませんということでしょう。",
        commentNum: 3,
        favoriteNum: 3,
        shareNum: 3
      }, {
        id: "demo03",
        users: [
          {id: "0000", name: "れんれん", icon: "renren"}
        ],
        title: "",
        writeTime: new Date,
        diffTime: "",
        content: "先日、「ゲイの友達が欲しい」と呟きましたが、正しくは「めっちゃ可愛い女の子の友達が欲しい」です。訂正してお詫び申し上げます。",
        commentNum: 3,
        favoriteNum: 3,
        shareNum: 3
      }, {
        id: "demo04",
        users: [
          {id: "0000", name: "れんれん", icon: "renren"}
        ],
        title: "久しぶりに8946の問題(Take#40, Take#61)を解いてみた",
        writeTime: new Date,
        diffTime: "",
        content: "久しぶりにまだ解いていなかった8946の問題を2問ほど解いてみました。以下にメモ代わりにネタバレコメントを残しておきますので、まだ解いてない人は注意してください。\n" +
        "Take#40\n" +
        "これ、思ったより解けてない人が多くて、すごく難しいのかと思ってました。ダウンロードするファイルはcapファイル。まあこの手の問題はWiresharkを使うのが手っ取り早いと思います。通信のやり取りを見ると、password.zipなるいかにもなファイルがあるので、File -> Export Objects -> HTTP でこれを抽出。\n" +
        "中身を見ようとするとパスがかかってて、ブルートフォースしてもいいんですが、他にヒントがないかもう一度capを調べる。中ではpingを二つのIPアドレスに飛ばしてるんだけど、片方からは応答がないし、ここら辺が怪しい。pingってデータの中身は適当らしいが…\n" +
        "そんなこんなでpassword.zipの中にあるパスワード計算.txtをゲットして、計算をするんですが、ここで結構つまづきました(笑)みなさん、計算はちゃんと厳密にやりましょう(笑)\n" +
        "Take#61\n" +
        "答えとなる文字列と、それを解読するためのテキストが与えられる。これは気付けば非常に単純だが、ちょっと英語が苦手だと無理かもしれません。あ、これが結構ヒントになってます(笑)\n" +
        "まず着目するのはaaaの文字列。検索してみると、かなりの頻度で出てきます。aaaみたいに並ぶ文字って、wwwぐらいしか見たことないんですが、どうもそんな感じではない。よくよく見ると、aaaの現れる前後にちょくちょくカンマやピリオド、数字がある。ここから閃くことが出来れば、あとは頑張ればいけます。",
        commentNum: 3,
        favoriteNum: 3,
        shareNum: 3
      }
    ];
    $scope.blogStreamItemParams = [];
    _.each(testData, (data:any) => {
      $scope.blogStreamItemParams.push(data);
    });
    _.each(testData, (data:any) => {
      $scope.blogStreamItemParams.push(data);
    });
    _.each(testData, (data:any) => {
      $scope.blogStreamItemParams.push(data);
    });
    _.each(testData, (data:any) => {
      $scope.blogStreamItemParams.push(data);
    });
    _.each(testData, (data:any) => {
      $scope.blogStreamItemParams.push(data);
    });

    $scope.loadMore = angular.bind(this, this.loadMore);
    $scope.getPath = angular.bind(this, this.getPath);
    $scope.infiniteItems = {
      numLoaded: 0,
      toLoad: 0,
      getItemAtIndex: (index:number):BlogStreamItemParam => {
        if(index > $scope.infiniteItems.numLoaded) {
          $scope.infiniteItems.fetchMoreItems(index);
          return null;
        }
        return $scope.blogStreamItemParams[index % 4];
        //return index;
      },
      getLength: ():number => {
        return $scope.infiniteItems.numLoaded + 1;
      },
      fetchMoreItems: (index:number):void => {
        if($scope.infiniteItems.toLoad < index) {
          $scope.infiniteItems.toLoad += 20;
          $timeout(angular.noop, 300).then(angular.bind(this, ():void => {
            $scope.infiniteItems.numLoaded = $scope.infiniteItems.toLoad;
          }));
        }
      }
    };
  }
  loadMore():string {
    var str:string = "call loadMore";
    console.log(str);
    return str;
  }
  getPath():string {
    return "testPath";
  }
}

var app = angular.module("app");
app.controller("blogTimelineController", ["$scope", "$timeout", BlogTimelineController]);
export = app;
