/**
 * Created by renma on 9/29/15.
 */

interface TabMenu {
  type:string;
  title:string;
  disabled:boolean;
}

interface Content {
  title:string;
  text:string;
  isDraft:boolean;
  chapterNum:number;
}

interface BlogFormScope {
  tabMenus:TabMenu[];
  murmur:Content;
  article:Content;
  submitMurmur:Function;
  submitArticle:Function;
}

class BlogFormController {

  constructor(private $scope: BlogFormScope) {
    $scope.tabMenus = [
      { type: "murmur", title: "つぶやき", disabled: false},
      { type: "article", title: "記事", disabled: false}
    ];

    $scope.murmur = <Content>{
      title: "", text: "", isDraft: false, chapterNum: 0
    };
    $scope.article = <Content>{
      title: "", text: "", isDraft: false, chapterNum: 0
    };

    $scope.submitMurmur = angular.bind(this, this.submitMurmur);
    $scope.submitArticle = angular.bind(this, this.submitArticle);
  }

  submitMurmur():void {
    console.log(this.$scope.murmur);
  }

  submitArticle():void {
    console.log(this.$scope.article);
  }
}

var app = angular.module("app");
app.controller("blogFormController", ["$scope", BlogFormController]);
export = app;
