/**
 * Created by renma on 9/5/15.
 */

interface BlogArticleScope {
  onClickSlidePrev:Function;
  tabMenus:string[];
}

class BlogArticleController {

  constructor(private $scope: BlogArticleScope) {
    $scope.onClickSlidePrev = angular.bind(this, this.onClickSlidePrev);

    $scope.tabMenus = ["タイムライン", "つぶやき", "記事"];
  }

  onClickSlidePrev():void {
    console.log("slidePrev");
  }
}

export = BlogArticleController;
