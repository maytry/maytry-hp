/**
 * Created by renma on 9/23/15.
 */

import _ = require("lodash");

interface User {
  name:string;
}

interface BlogStreamItemParam {
  users:User[];
}

interface BlogStreamItemScope {
  joinUserNames:Function;
}

class BlogStreamItemController {
  constructor(private $scope:BlogStreamItemScope) {
    $scope.joinUserNames = angular.bind(this, this.joinUserNames);
  }

  joinUserNames(param:BlogStreamItemParam):string {
    return _.map(param.users, (user:User) => {
      return user.name;
    }).join();
  }
}

var app = angular.module("app");
app.controller("blogStreamItemController", ["$scope", BlogStreamItemController]);
export = app;
