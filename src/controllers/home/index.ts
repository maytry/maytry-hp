/**
 * Created by renma on 9/5/15.
 */

interface HomeScope {
  myInterval:number;
  noWrapSlides:boolean;
  slides:string[];
  addSlide:Function;
}

class HomeController {

  constructor(private $scope:HomeScope) {
    $scope.myInterval = 5000;
    $scope.noWrapSlides = false;
    var slides = $scope.slides = [];
    slides.push({
      image: "images/icons/title_logo.png",
      text: "title"
    });
    $scope.addSlide = function() {
      var newWidth = 600 + slides.length + 1;
      slides.push({
        image: "http://placekitten.com/" + newWidth + "/300",
        text: ["More", "Extra", "Lots of", "Surplus"][slides.length % 4] + " " +
        ["Cats", "Kittys", "Felines", "Cutes"][slides.length % 4]
      });
    };
    for (var i = 0; i < 4; i++) {
      $scope.addSlide();
    }
  }
}

var app = angular.module("app");
app.controller("homeController", ["$scope", HomeController]);

export = app;
