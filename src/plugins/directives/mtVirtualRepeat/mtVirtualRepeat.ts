interface MtVirtualRepeatScope {
  param:string;
}

class MtVirtualRepeat implements ng.IDirective {
  restrict:string;
  replace:boolean;
  transclude:boolean;
  scope:MtVirtualRepeatScope;
  controller:any;
  compile:ng.IDirectiveCompileFn;
  template:string;

  constructor($window:any, $compile:any) {
    this.restrict = "EA";
    this.replace = false;
    this.transclude = false;
    this.scope = <MtVirtualRepeatScope>{
      param: "="
    };
    this.controller = ["$scope", ($scope:any) => {
      /** 繰り返し要素のリスト */
      $scope.repeatList = [];
      /** 繰り返し要素 */
      $scope.repeat = null;
      /** 繰り返し要素のリスト名 */
      $scope.repeatListExpression = "";
      /** 繰り返し要素名 */
      $scope.repeatName = "";
      /** 表示件数情報 */
      $scope.displayInfo = {
        /** 最初に表示する件数 */
        initialNum: 4,
        /** 読み込み件数 */
        reloadNum: 4,
        /** 現在表示しているリストの表示のスタート位置 */
        startIndex: 0,
        /** 現在表示しているリストの表示のラスト位置 */
        lastIndex: 0
      };
        /**  */
      $scope.loadMore = angular.bind(this, this.loadMore);
    }];
    this.compile = ($elem:any, $attr:any) => {
      var expression:string = $attr.mtVirtualRepeat;
      /** param in params のような形にマッチする */
      var match:RegExpMatchArray = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)\s*$/);
      // TODO: Error処理
      var repeatName:string = match[1];
      var repeatListExpression:string = match[2];

      return ($scope:any, $elem:any, $attr:any, $ctrl:any) => {
        var raw = $elem[0];
        $elem.bind("scroll", () => {
          if(raw.scrollHeight <= raw.offsetTop + raw.offsetHeight) {
            $elem.append("<div>append test</div>");
            console.log("call scroll");
          }
        });
        $scope.repeatList = $scope.$parent[repeatListExpression];
        if($scope.repeatList == null) {
          return;
        }
        var dispInfo = $scope.displayInfo;
        dispInfo.lastIndex = Math.min(dispInfo.reloadNum, $scope.repeatList.length - dispInfo.lastIndex + 1) - 1;
        $scope.repeatName = repeatName;
        $scope.repeatListExpression = repeatListExpression;
        var str = $elem[0].innerHTML;
        $elem[0].innerHTML = "";
        for(var i = dispInfo.startIndex; i < dispInfo.lastIndex + 1; i++) {
          $elem.append(str.replace(repeatName, "repeatList[" + i + "]"));
          $compile($elem.contents())($scope);
        }
      };
    };
  }

  /**  */
  loadMore():string {
    var str:string = "call loadMore";
    console.log(str);
    return str;
  }
}

var app = angular.module("app");
app.directive("mtVirtualRepeat", ($window:any, $compile:any):MtVirtualRepeat => new MtVirtualRepeat($window, $compile));
export = app;
