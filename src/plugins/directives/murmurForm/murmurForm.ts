interface MurmurFormScope {
  param:string;
}

class MurmurForm implements ng.IDirective {
  restrict:string;
  replace:boolean;
  scope:MurmurFormScope;
  templateUrl:string;

  constructor() {
    this.restrict = "EA";
    this.replace = true;
    this.scope = <MurmurFormScope>{
      param: "="
    };
    this.templateUrl = "plugins/directives/murmurForm.html";
  }
}

var app = angular.module("app");
app.directive("murmurForm", ():MurmurForm => new MurmurForm());
export = app;
