class SlideNavSet implements ng.IDirective {
  restrict: string;
  replace: boolean;
  template: string;

  constructor() {
    this.restrict = "EA";
    this.replace = true;
    this.template =
      "<div>" +
        "<a class='fa fa-angle-double-left slide-prev' ng-click='onClickSlidePrev()'></a>" +
        "<a class='fa fa-angle-double-right slide-next' ng-click='onClickSlideNext()'></a>" +
        "<div class='slide-nav'>" +
          "<a class='fa fa-circle-o' href='#'></a>" +
          "<a class='fa fa-circle-o fa-dot-circle-o slide-nav-active'></a>" +
        "</div>" +
      "</div>";
  }
}

var app = angular.module("app");
app.directive("slideNavSet", ():SlideNavSet => new SlideNavSet());
export = app;
