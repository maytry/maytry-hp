class StageNavNext implements ng.IDirective {
  restrict: string;
  replace: boolean;
  template: string;

  constructor() {
    this.restrict = "EA";
    this.replace = true;
    this.template =
      "<div class=\"stage-next\">" +
        "<span>cd ./special_BLOG</span>" +
        "<a class=\"fa fa-angle-double-down\" ng-href=\"#/blog/timeline\"></a>" +
      "</div>";
  }
}

var app = angular.module("app");
app.directive("stageNavNext", ():StageNavNext => new StageNavNext());
export = app;
