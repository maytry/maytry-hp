interface User {
  id:string;
  name:string;
  icon:string;
}

interface BlogStreamItemParam {
  id:string;
  users:User[];
  title:string;
  writeTime:Date;
  diffTime:string;
  content:string;
  commentNum:number;
  favoriteNum:number;
  shareNum:number;
}

interface BlogStreamItemScope {
  param:string;
}

class BlogStreamItem implements ng.IDirective {
  restrict:string;
  replace:boolean;
  scope:BlogStreamItemScope;
  link:any;
  controller:any;
  templateUrl:string;

  constructor() {
    this.restrict = "EA";
    this.replace = true;
    this.scope = <BlogStreamItemScope>{
      param: "="
    };
    this.controller = ["$scope", function($scope:any) {
      $scope.getBlogStreamItemPath = angular.bind(this, this.getBlogStreamItemPath);
    }
  ];
    this.templateUrl = "plugins/directives/blogStreamItem/blogStreamItem.html";
  }

  getBlogStreamItemPath(id:string):string {
    return "dist/markdowns/tmp/" + id + ".html";
  }
}

var app = angular.module("app");
app.directive("blogStreamItem", ():BlogStreamItem => new BlogStreamItem());
export = app;
