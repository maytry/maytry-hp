import _ = require("lodash");

interface User {
  name:string;
}

class JoinNamesFilter {
  constructor(users:User[]) {
    return _.map(users, (user:User) => {
      return user.name;
    }).join(", ");
  }
}

var app = angular.module("app");
app.filter("joinNames", () => JoinNamesFilter);
export = app;
