class Genre {
  constructor(
    public id:number,
    public name:string,
    public subGenre:Genre,
    public selected:boolean)
  {
    this.id = id;
    this.name = name;
    this.subGenre = subGenre;
    this.selected = selected;
  }
}

export = Genre;
