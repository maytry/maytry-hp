import Genre = require("../fragments/genre");

class GenreService {
  /** 登録されている全てのジャンル */
  allGenre =  [
    new Genre(1, "CTF", new Genre(1001, "ksnctf", null, false), false),
    new Genre(2, "ファッション", null, false),
    new Genre(3, "日常", null, false),
    new Genre(4, "恋愛", null, false),
    new Genre(5, "テクノロジー", null, false)
  ];

  /** 選択されているジャンル */
  selectedGenres = [];
}

var app = angular.module("app");
app.factory("genreService", () => new GenreService());

export = app;
